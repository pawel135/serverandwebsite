#-*-coding:utf-8-*-
import socket
import re
import datetime

def fileContentAndSize( filename ):
  f = open(filename,"rb")
  resource = f.read()
  f.seek(0,2) # file object poisition moved to 0 bytes from the end
  size = f.tell() # position of file object
  f.close()
  return [ resource, size ]

def appendHeadersAndContent( contentType, contentSize, resource ):
    http_responseAppend = ""
    http_responseAppend += "Content-Type:" + contentType + "\n"
    http_responseAppend += "Content-Length: " + str(contentSize)  + "\n"
    http_responseAppend += "\n"
    http_responseAppend += resource
    return http_responseAppend;

def resourceBasedOnRequestLanguageAccept( requestStr, lang, isPlainText): 
  http_response = ""
  resource = ""
  
  if ( re.compile("(?i)(.*.html|.*.htm)$").match(requestStr) ):
    if "pl"==lang and False == isPlainText:
        requestParts = requestStr.split(".");
        [resource, size] = fileContentAndSize( requestParts[0:-1]+"_pl."+requestParts[-1] );
    elif False == isPlainText:
      [resource, size] = fileContentAndSize( requestStr );
    else:
      requestParts = requestStr.split(".");
      [resource, size] = fileContentAndSize( requestParts[0:-1]+"_raw."+requestParts[-1] );
    return appendHeadersAndContent( "text/html; charset=utf-8", size, resource )
  
  elif ( re.compile("(\s*|index|index.htm(l)?|main)$").match(requestStr) ):
    [resource, size] = fileContentAndSize( "index.html" );
    return appendHeadersAndContent( "text/html; charset=utf-8", size, resource )
  
  elif ( re.compile("(?i)(.*.css)$").match(requestStr)):
    [resource, size] = fileContentAndSize( requestStr );
    return appendHeadersAndContent( "text/css", size, resource )
  
  elif ( re.compile("(?i)(.*.js)$").match(requestStr)):
    [resource, size] = fileContentAndSize( requestStr ); 
    return appendHeadersAndContent( "application/javascript", size, resource )
  
  elif ( re.compile("(?i)(.*.webm)$").match(requestStr)):
    [resource, size] = fileContentAndSize( requestStr ); 
    return appendHeadersAndContent( "video/webm", size, resource )
  
  elif ( re.compile("(?i)(.*.mp3)$").match(requestStr)):
    requestSplitted = requestStr.split(".")
    http_response += "Content-Disposition: filename=\""+ ( "".join(requestSplitted[0:-1]) + "." + requestSplitted[-1].upper() ) +"\"\n"
    [resource, size] = fileContentAndSize( requestStr ); 
    http_response += appendHeadersAndContent( "audio/mpeg", size, resource )
    return http_response
  
  elif ( re.compile("(?i)(.*.png|.*.jpg|.*.gif)$").match(requestStr)):
    type = requestStr.split(".")[-1]
    if ( re.compile("jpg").match(type) ):
      type = type.replace("jpg","jpeg")
    [resource, size] = fileContentAndSize( requestStr ); 
    return appendHeadersAndContent( "image/"+ type, size, resource )
  
  elif ( re.compile("(?i)(.*.svg)$").match(requestStr)):
    [resource, size] = fileContentAndSize( requestStr ); 
    return appendHeadersAndContent( "image/"+ requestStr.split(".")[-1]+"+xml", size, resource )
 
  elif ( re.compile("(?i)(time|date|now)$").match(requestStr)):
    http_response += "Content-Type:text/html;\n\n";
    return http_response + formatTimeInLanguage(lang)
  
  elif ( re.compile("(?i)(.*contact)$").match(requestStr)):
    http_response += "Content-Type:text/html;\n\n";
    return http_response + getContactInfoInLanguage(lang)
  
  else:
    return ""

def formatTimeInLanguage(lang): 
  now = datetime.datetime.now()  
  months_pl = ["styczeń", "luty", "marzec", "kwiecień", "maj", "czerwiec", "lipiec", "sierpień", "wrzesień", "grudzień"]
  weekdays_pl = ["niedziela","poniedziałek","wtorek","środa","czwartek","piątek","sobota"]
 
  months_en = ["January","February","March","April","May","June","July","August","Semptember","October","November","December"]
  weekdays_en = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]
  
  if ( lang == "en"):
    return str(now.hour) + " " + weekdays_en[int(now.strftime("%w"))] + ", " + str(now.day) + \
      " " +  months_en[now.month] + " " + now.strftime("%Y %H:%M:%S")
  elif( lang == "pl"):
    return str(now.hour) + " " + weekdays_pl[int(now.strftime("%w"))] + ", " + str(now.day) + \
      " " +  months_pl[now.month] + " " + now.strftime("%Y %H:%M:%S")
  
  return ""

def getContactInfoInLanguage(lang): 
  f = ""
  if  lang == "pl" :  
    try:
      f = open("contact_pl.txt","rb")
    except IOError:
      return "Nie można wyświetlić informacji kontaktowych"  
  else: 
    try:
      f = open("contact_en.txt","rb")
    except IOError:
      return "Cannot display contact info"  
  resource = f.read()
  f.close()
  return resource;


def main():
  HOST, PORT = '', 8011
  listen_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  listen_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
  listen_socket.bind((HOST, PORT))
  listen_socket.listen(1)
  print 'Serving HTTP on port %s ...' % PORT
  while True:
    client_connection, client_address = listen_socket.accept()
    print client_address
    request = client_connection.recv(1024)
    print request
    request_lines=request.split('\n')
    http_response = """\
HTTP/1.1 200 OK
"""
    language="en"
    requestStr=""
    isPlainText = False
    for line in request_lines:
      if 'GET' in line:
        requestStr = line.split(" ")[1][1:] # 2nd element, removed 1st char
        print line
        print requestStr
     # elif 'POST' in line:
     # TODO: possible POST service??  
      elif 'Accept' in line:
       isPlainText = re.compile("\s*text/plain").match(line.split(":")[1])
      elif 'Accept-Language' in line:
        headerStr = line.split(":")[1].partition(";")[0] # take 2nd element after split, take the first part of 2 (divided by ';')
        print headerStr
        for langPreference in headerStr.split(","):
          if ( re.compile(".*en").match(langPreference)):
            language = "en" 
            break
          elif ( re.compile(".*pl").match(langPreference)):
            language = "pl"
          break
  
  
    print "[LANGUAGE]: " + language
    resource_response = resourceBasedOnRequestLanguageAccept(requestStr,language,isPlainText)
    if ( len(resource_response)  < 1 ) :
      resource_response = "\nNo such resource" 
  
    http_response += resource_response;
    client_connection.sendall(http_response)
    client_connection.close()

if __name__ ==  '__main__':
  main()

