var author = "Paweł Szczepanowski";
var hexDigits= new Array("0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f");
var bgButton = document.getElementById("toggleBgAudioButton"),
  buttonStyle = window.getComputedStyle(bgButton),
  bgButtonColor = buttonStyle.getPropertyValue('background-color'),
  bgButtonImage = buttonStyle.backgroundImage.slice(4,-2).split(/:\d+/)[1];
function toggleBgSound(){
 var vid = document.getElementById("bgSound");
 if ( vid.paused ){
   relaunchBgSound();
 }
 else {
   stopBgSound(bgButtonColor,bgButtonImage);
 }
 
}

function relaunchBgSound(){
  var bg = document.getElementById("bgSound"); 
  bg.play();
//  bgButton.style.background = "#ffaaaa"; // light red
//  bgButton.style.backgroundImage = "url('../image/stop.png')";
  bgButton.setAttribute("style","background-image: url('../image/stop.png'); background-repeat: no-repeat; size:30px");
  //  $("#toggleBgAudioButton").css("background-color","red");
//  bgButton.innerHTML="Stop Background Sound";
}

function stopBgSound(bgColor,bgImage){
  var bg = document.getElementById("bgSound"); 
  bg.pause();
//  bgButton.style.background = rgbToHex(bgColor);
// $("#toggleBgAudioButton").css("background-color","green");
//  bgButton.style.backgroundImage = "url('.." + bgImage + "')"; 
  bgButton.setAttribute("style","background-image: url('.." + bgImage + "'); background-repeat: no-repeat; size:30px");
  
//document.getElementById("toggleBgAudioButton").innerHTML="Relaunch Background Sound";
}

function rgbToHex(rgb){
  rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
    return "#"+hex(rgb[1])+hex(rgb[2])+hex(rgb[3]);
}

function hex(n){
  return isNaN(n) ? "00" : hexDigits[(n-n%16)/16] + hexDigits[n%16];
}

document.getElementById("header1").innerHTML = author;
//document.getElementById("p_first").innerHTML = "Hello " + prompt_user_name; // from scriptHead.js 


document.getElementById("toggleBgAudioButton").addEventListener("click", toggleBgSound); // if stopBgSound is passed with (), the method is not passed, but ratther it's only executed right away



